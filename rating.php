<?php 

function rating($voted=true) {
    $id = get_the_ID();
    if($voted) {
        $disable_class = (isset($_COOKIE["vote-post-". $id]) ?
                            (!!$_COOKIE["vote-post-". $id] ? ' disabled' : '') : '');
    } else {
        $disable_class = ' disabled';
    }
    $total      = wp__get_data('vote-total', $id);
    $rating     = wp__get_data('vote-rating', $id);
    $total_text = sklonen($total, 'голос', 'голоса', 'голосов', true);
    $total_rec  = $total;
    $title      = get_the_title();
    $author     = get_the_author();
    $permalink  = get_the_permalink();

    $pr  = ($total > 0 ? ($rating/($total*5))*100 : 0);
    $abs = ($total > 0 ? round($rating/$total, 1) : 0);

    $ratingHTML = '
        <ol class="rating show-current">
            <li>5</li><li>4</li><li>3</li><li>2</li><li>1</li>
            <li class="currentbox"><span style="width:'.$pr.'%"></span></li>
        </ol>';

    // $richSnp = '<div typeof="v:Rating"><div itemprop="AggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating"><meta itemprop="itemReviewed" content="' . $title . '"> <meta itemprop="name" content="' . $title . '"><meta itemprop="bestRating" content="5"><meta itemprop="worstRating" content="1"><meta property="v:rating" content="'.($abs).'" /><meta itemprop="ratingValue" content="'.($abs).'"><meta itemprop="ratingCount" property="v:votes" content="'.$total.'"></div></div>';
    $result = '
        <div class="vote-block'. $disable_class .'" data-id="'. $id .'" data-total="'. $total_rec .'" data-rating="'. $rating .'" rel="v:rating">'.$ratingHTML.'</div>
        <div class="rating-info" id="rating-info"></div>
        <div class="rating-text"  id="rating-text">('.$total_text.', среднее: '.($abs).' из 5)</div>';

    if (has_excerpt()) {
        $excerpt = get_the_excerpt();
        $excerpt = str_replace('"', '', $excerpt);
        $excerpt = strip_tags($excerpt);
    } else {
        $excerpt = mb_substr( strip_tags( get_the_content() ), 0, 50 );
        $excerpt = str_replace('"', '', $excerpt);
        $excerpt = strip_tags($excerpt);
    }
    if ($total > 0) {
        $result .= '
        <script type="application/ld+json">
        {
        "@context": "http://schema.org/",
        "@type": "AggregateRating",
        "itemReviewed": "' . $title . '",
        "author": "'. $author .'",
        "description": "'. $excerpt .'",
        "mainEntityOfPage": "'. $permalink .'",
        "ratingCount": "'. $total .'",
        "bestRating": "5",
        "ratingValue": "'. $abs .'",
        "worstRating": "1",
        "name": "' . $title . '"
        }
        </script>';
    }
    return $result;
}

function sklonen($n,$s1,$s2,$s3, $b = false){
    $m = $n % 10; $j = $n % 100;
    if($m==0 || $m>=5 || ($j>=10 && $j<=20)) return $n.' '.$s3;
    if($m>=2 && $m<=4) return  $n.' '.$s2;
    return $n.' '.$s1;
}

function wp__set_data($name, $postID, $value) {
    $count_key = $name;
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        update_post_meta($postID, $count_key, $value);
    }
}
function wp__get_data($name, $postID){
    $count_key = $name;
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count.'';
}

function rating_ajax($id, $voted=true) {
    if ($voted) {
        $disable_class = (isset($_COOKIE["vote-post-". $id]) ?
                            (!!$_COOKIE["vote-post-". $id] ? ' disabled' : '') : '');
    } else {
        $disable_class = ' disabled';
    }
    $total  = wp__get_data('vote-total', $id);
    $rating = wp__get_data('vote-rating',$id);

    $total_text = sklonen($total, 'голос', 'голоса', 'голосов', true);
    $total_rec  = $total;
    $title = get_the_title();

    // if ($total==0) {$total = 1;}

    $pr  = ($total > 0 ? ($rating/($total*5))*100 : 0);
    $abs = ($total > 0 ? round($rating/$total, 1) : 0);

    $detect = new Mobile_Detect;
    $width  = ($detect->isMobile() && $disable_class != ' disabled' ? 0 : $pr);

    $ratingHTML = '
        <ol class="rating show-current">
            <li>5</li><li>4</li><li>3</li><li>2</li><li>1</li>
            <li class="currentbox"><span style="width:'.$width.'%"></span></li>
        </ol>';

    $richSnp = '
        <div typeof="v:Rating">
            <div itemprop="AggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                <meta itemprop="itemReviewed" content="' . $title . '">
                <meta itemprop="name" content="' . $title . '">
                <meta itemprop="bestRating" content="5">
                <meta itemprop="worstRating" content="1">
                <meta property="v:rating" content="'.($abs).'" />
                <meta itemprop="ratingValue" content="'.($abs).'">
                <meta itemprop="ratingCount" property="v:votes" content="'.$total.'">
            </div>
        </div>';

    $result = '
        <div class="vote-block'. $disable_class .'" data-id="'. $id .'" data-total="'. $total_rec .'" data-rating="'. $rating .'" rel="v:rating">'.$richSnp.''.$ratingHTML.'</div>
        <div class="rating-info" id="rating-info">Вы проголосовали</div>
        <div class="rating-text"  id="rating-text">('.$total_text.', среднее: '.($abs).' из 5)</div>';


    return $result;
}
