<?php
/*
Plugin Name: Modesco Author Box
Description: Блок автора и получение рейтинга
Version: 4.9.1
*/

define('BOX_DIR', dirname(__FILE__));
if (!class_exists('Mobile_Detect')) {
    require_once 'Mobile_Detect.php';
}
include_once(BOX_DIR.'/rating.php');

/**
 * Добавление блока звезд
 * @param  [string] $content - исходный контент
 * @return [string]          - контент с добавленными блоками
 */
function add_author_box( $content ) {
    if (is_main_query() && is_singular('post')) {
        global $authordata;
        global $post;
        $authorid = get_the_author_meta('ID');

        if ($authorid == '') {
            $author = $authordata;
        } else {
            $author = get_userdata($authorid);
        }

        $is_author_description = get_the_author_meta('description');
        $more_author_posts     = get_author_posts_url($author->ID);
        $authorName            = get_the_author();
        $authorUrl             = get_author_posts_url(get_the_author_meta('ID'));
        $pos1                  = strpos(get_the_author_meta('description'),"\n");

        if ($pos1 === false) {
            $authorDescriptionShort = $is_author_avatar = '';
        } else {
            $authorDescriptionShort = strstr(get_the_author_meta('description'),"\n",true);

            if (!strpos($ava = get_avatar($author->ID), 'avatar-default')) {
                $is_author_avatar = '<div class="box-avatar">'. $ava .'</div>';
            } else {
                $is_author_avatar = '';
            }
        }      
        $rating = "rating";

        $notification = '';
        $box_fab = '
            <div class="main-box" id="print-content">
                <ul class="box-fab-list">
                    <li class="box-fab-search-author-link">Автор: <a href="'. $authorUrl .'" rel="nofollow">'. $author->display_name .'</a></li>'. $notification .'
                    <li class="box-fab-search-author-link"><a class="btn-print" href="javascript:window.print()" rel="nofollow" ><i class="fa fa-print" aria-hidden="true"></i>  Распечатать</a></li>
                </ul>';
        $author_box = '
                <div class="box_author">'.  $is_author_avatar .'
                    <div class="box-option"><span class="description">'.  $authorDescriptionShort . '</span>
                        <div class="p_bold"> 
                            <span class="rating">Оцените статью:</span>
                            <div class="tst_rating"></div>
                            <div id="md_author__ratingstars">'. $rating() .'</div>
                            <span id="md_author__labelshared" class="rating">Поделитесь с друзьями!</span>
                        </div>
                        <div class="other"> 
                            <div class="link">
                                <div class="share">
                                    <span class="link-more"><!-- uSocial --><script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
                                    </span>
                                        <div class="uSocial-Share" data-pid="2d516df27f5f5a5408b4f6d3a6846a1e" data-type="share" data-options="rect,style1,default,absolute,horizontal,size48,eachCounter0,counter0,nomobile" data-social="vk,fb,twi,ok,telegram"></div><!-- /uSocial -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';

        $box_fab_end = '</div>';

/**
* Вывод области виджетов after_author_box
*/
    ob_start();
    if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('after_author_box') ) : endif;
    $add_after_author_box = ob_get_contents();
    ob_get_clean();
    $author_box .= $add_after_author_box;

        return $content . $box_fab . $author_box . $box_fab_end;
    }
    return $content;
}
add_filter('the_content', 'add_author_box', 20);

/**
 * Область виджетов after_author_box
 */
function register_after_author_box(){
  register_sidebar( array(
    'name' => "after_author_box",
    'id' => 'after_author_box',
    'description' => 'После блока автора',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
  ) );
}
add_action( 'widgets_init', 'register_after_author_box');

/**
 * Регистрация скриптов, локализация скрипта для AJAX запросов
 */
function raiting_js() {
    // if (isset($_GET['debug'])) {
        // wp_register_script( 'jquery_rating', plugins_url( '/js/rating.js', __FILE__ ),false, '1.0',  true);  // для дебаггинга
    // } else {
        wp_register_script( 'jquery_rating', plugins_url( '/js/rating.min.js', __FILE__ ),false, '1.0',  true);
    // }

    wp_enqueue_script('jquery_rating');

    wp_localize_script(
      'jquery_rating',
      'modesco_ajax',
      array(
        'url'     => admin_url('admin-ajax.php'),
        'nonce'   => wp_create_nonce('modesco-nonce'),
        'post_id' => get_the_ID(),
      )
    );


}  
add_action( 'wp_enqueue_scripts', 'raiting_js' );

/**
 * Подключение стилей
 */
function add_scripts_styles() {
    // if (isset($_GET['debug'])) {
        // $css_url = plugins_url("css/box_style.css", __FILE__);
    // } else {
        $css_url = plugins_url("css/box_style.min.css", __FILE__);
    // }
    wp_register_style('box_css', $css_url, '');
    wp_enqueue_style('box_css');
}
add_action('wp_enqueue_scripts', 'add_scripts_styles', 5);

add_action('wp_head', function(){
    ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <?php
});

/**
 * ajax звезд при загрузке страницы
 */
add_action( 'wp_ajax_modesco_author_box__get_rating', 'modesco_author_box__get_rating' );
add_action( 'wp_ajax_nopriv_modesco_author_box__get_rating', 'modesco_author_box__get_rating' );

function modesco_author_box__get_rating() {
    // if ( ! wp_verify_nonce( htmlentities( $_POST['nonce'] ), 'modesco-nonce' ) ) {
    //   echo 'error';
    //   wp_die();
    // }
    require_once dirname(__FILE__) .'/rating.php';
    $post_id = esc_html($_POST['post_id']);
    $rating  = rating_ajax($post_id);

    echo $rating;
    wp_die();
}


/**
 * ajax для звезд при голосовании
 */
add_action( 'wp_ajax_modesco_author_box__set_rating', 'modesco_author_box__set_rating' );
add_action( 'wp_ajax_nopriv_modesco_author_box__set_rating', 'modesco_author_box__set_rating' );

function modesco_author_box__set_rating() {
    // if ( ! wp_verify_nonce( htmlentities( $_POST['nonce'] ), 'modesco-nonce' ) ) {
    //   echo 'error';
    //   wp_die();
    // }
    require_once dirname(__FILE__) .'/rating.php';

    $post_id = esc_html($_POST['post_id']);
    $num = esc_html($_POST['num']);

    if(!$_COOKIE["vote-post-".$post_id]) {
        wp__set_data('vote-total',$post_id,(int)wp__get_data('vote-total',$post_id) + 1);
        wp__set_data('vote-rating',$post_id,(int)wp__get_data('vote-rating',$post_id) + $num);

        $total = wp__get_data('vote-total',$post_id);
        $rating = wp__get_data('vote-rating',$post_id);

        if($total==0) {$total = 1;}

        $result = ($rating/($total*5))*100;
    } else {
        $result = 'limit';
    }

    echo $result;
    wp_die();
}