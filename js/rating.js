(function($){
    'use strict';
    $(document).on('ready',function(){
        var data = {
            action: 'modesco_author_box__get_rating',
            post_id: modesco_ajax.post_id,
        }
        $.post(modesco_ajax.url, data, function(){})
        .success(function(data){$('#md_author__ratingstars').html(data)});


        $(this).on('click','#print',function() {
            var prtContent = document.getElementById('content_wrap').innerHTML;
            var WinPrint = window.open('','','left=50,top=50,width=800,height=640,toolbar=0,scrollbars=1,status=0'); 
            WinPrint.document.write('<div id="print" class="contentpane">'); 
            WinPrint.document.write(prtContent); 
            WinPrint.document.write('</div>'); 
            WinPrint.document.close(); 
            WinPrint.focus(); 
            WinPrint.print(); 
            WinPrint.close(); 
            prtContent.innerHTML=strOldOne;
        })
        .on('mouseover','.vote-block li',function() {
            var $el = $(this);
            var star = parseInt($el.text(),10);

            if($el.parent().parent().hasClass('disabled')) {
                // $('#rating-text').css('display', 'none');
                return false;
            }
        })
        .on('mouseleave','.vote-block li',function() {
            // $('#rating-text').css('display', 'none');
        })
        .on('click','.vote-block li',function() {

            var $el = $(this);
            var id = modesco_ajax.post_id;
            var total = $el.parent().parent().data('total');
            var rating = $el.parent().parent().data('rating');
            var num = parseInt($el.text(),10);

            if($el.parent().parent().hasClass('disabled')) {
                return false;
            }

            var data = {
                action: 'modesco_author_box__set_rating',
                post_id: modesco_ajax.post_id,
                num:     num
            }
            $.post(modesco_ajax.url, data, function(){})
            .success(function(data){
                if (data === 'limit') {
                    return false;
                } else {
                    $el.parent().parent().addClass('disabled');
                    $.cookie('vote-post-'+id, true, {expires: 7, path: '/' });
                    $el.parent().find('.currentbox span').css('width',data+'%');
                    total++;
                    var abs = ((rating+num)/total);
                    abs = (abs^0)===abs?abs:abs.toFixed(1);
                    $('.rating-text').html('('+total+' '+decOfNum(total, ['звезда', 'звезды', 'звёзд']) +', в среднем: '+abs+' из 5)');
                }
            });
            return false;
        })
    })
    function decOfNum(number, titles) {  
        var cases = [2, 0, 1, 1, 1, 2];  
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
    }
})(jQuery);

! function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : jQuery)
}(function(e) {
    function n(e) {
        return u.raw ? e : encodeURIComponent(e)
    }

    function o(e) {
        return u.raw ? e : decodeURIComponent(e)
    }

    function i(e) {
        return n(u.json ? JSON.stringify(e) : e + "")
    }

    function r(e) {
        0 === e.indexOf('"') && (e = e.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\"))
        try {
            return e = decodeURIComponent(e.replace(c, " ")), u.json ? JSON.parse(e) : e
        } catch (n) {}
    }

    function t(n, o) {
        var i = u.raw ? n : r(n)
        return e.isFunction(o) ? o(i) : i
    }
    var c = /\+/g,
        u = e.cookie = function(r, c, a) {
            if (arguments.length > 1 && !e.isFunction(c)) {
                if (a = e.extend({}, u.defaults, a), "number" == typeof a.expires) {
                    var f = a.expires,
                        s = a.expires = new Date
                    s.setTime(+s + 864e5 * f)
                }
                return document.cookie = n(r) + "=" + i(c) + (a.expires ? "; expires=" + a.expires.toUTCString() : "") + (a.path ? "; path=" + a.path : "") + (a.domain ? "; domain=" + a.domain : "") + (a.secure ? "; secure" : "")
            }
            for (var d = r ? void 0 : {}, p = document.cookie ? document.cookie.split("; ") : [], m = 0, x = p.length; x > m; m++) {
                var l = p[m].split("="),
                    k = o(l.shift()),
                    v = l.join("=")
                if (r && r === k) {
                    d = t(v, c)
                    break
                }
                r || void 0 === (v = t(v)) || (d[k] = v)
            }
            return d
        }
    u.defaults = {}, e.removeCookie = function(n, o) {
        return void 0 === e.cookie(n) ? !1 : (e.cookie(n, "", e.extend({}, o, {
            expires: -1
        })), !e.cookie(n))
    }
})